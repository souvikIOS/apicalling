//
//  LandingViewController.swift
//  TestProject1
//
//  Created by Souvik on 09/01/21.
//

import UIKit

class LandingViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var dataModel = LandingData()
    var objData : RoleListDataModel?
    var isHeaderElaborated : Bool = false
    var isCellElaborated : Bool = false
    var elaboratedSection : Int = -1
    var elaboratedRow : Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "RecursiveCell", bundle: nil), forCellReuseIdentifier: RecursiveCell.identifier)
        self.tableView.register(UINib(nibName: "InnerCell", bundle: nil), forCellReuseIdentifier: InnerCell.identifier)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        dataModel.getAllDataFromResponse { (responseData) in
            self.objData = responseData
//            let dt = (self.objData?.data![0])!
//            self.objData?.data?.append(dt)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    

  
}
//MARK : - IBActions -

extension LandingViewController{
    @objc func headerDidClick(_ sender : UIButton){
        isHeaderElaborated = !isHeaderElaborated
        isCellElaborated = false
        if isHeaderElaborated{
            self.elaboratedSection = sender.tag

        }else{
            self.elaboratedSection = -1
        }
        self.tableView.reloadData()
    }
}


//MARK : - Uitableview datasource and delegate -
extension LandingViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.objData?.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.tableView.frame.size.width, height: 40.0))
        let sepView = UIView(frame: CGRect(x: 0.0, y: 39.0, width: self.tableView.frame.size.width, height: 1.0))
        let dotView = UIView(frame: CGRect(x: 0.0, y: 13.0, width: 10.0, height: 10.0))
        dotView.layer.cornerRadius = 5.0
        dotView.layer.masksToBounds = true
        let label = UILabel(frame: CGRect(x: 20.0, y: 0.0, width: self.tableView.frame.size.width, height: 40.0))
        let button = UIButton(frame: view.bounds)
        label.text = self.objData?.data?[section].roleName ?? ""
        label.textColor = UIColor.white
        button.tag = section
        button.addTarget(self, action: #selector(headerDidClick(_:)), for: .touchUpInside)
        view.addSubview(label)
        view.addSubview(button)
        if section == elaboratedSection{
            sepView.backgroundColor = .clear
            dotView.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        }else{
            sepView.backgroundColor = .white
            dotView.backgroundColor = .clear
        }
        view.addSubview(dotView)
        view.addSubview(sepView)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isHeaderElaborated{
            if section == elaboratedSection{
                if isCellElaborated{
                    return 1 + (self.objData?.data?[section].planning?.objective?.count ?? 0)
                }else{
                  return 1
                }
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isCellElaborated{
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: RecursiveCell.identifier) as! RecursiveCell
                cell.planningData = self.objData?.data?[indexPath.section].planning
                return cell

            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: InnerCell.identifier) as! InnerCell
                cell.objectiveData = self.objData?.data?[indexPath.section].planning?.objective?[indexPath.row-1]
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: RecursiveCell.identifier) as! RecursiveCell
            cell.planningData = self.objData?.data?[indexPath.section].planning
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.isCellElaborated = !self.isCellElaborated
        self.elaboratedRow = indexPath.row
        self.tableView.reloadSections(IndexSet(integersIn: indexPath.section...indexPath.section), with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
}
