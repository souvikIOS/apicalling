//
//  LandingData.swift
//  TestProject1
//
//  Created by Souvik on 09/01/21.
//

import Foundation

class LandingData{
    let webManager = WebServiceManager()
    func getAllDataFromResponse(completion : @escaping(_ resData : RoleListDataModel?) -> Void){
        let header : [String: String] = ["Content-Type" : "application/json",
                                     "Accept" : "application/json",
                                     "Authorization": "Bearer 739|c0DxqQa9nURCa2OSMMgxAdyEMdLre0JCscuXpTYz"]

        self.webManager.requestAPI(url: "http://planningpro.dedicateddevelopers.us/api/role/planning/0", httpHeader: header) { (resData, error) in
            completion(resData)
        }
    }
}
