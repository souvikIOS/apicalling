//
//  RecursiveCell.swift
//  TestProject1
//
//  Created by Souvik on 09/01/21.
//

import UIKit

class RecursiveCell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var planningData : Planning?{
        didSet {
            labelTitle.text = "Role \(planningData?.id ?? 0)"
        }
        
    }
    
}

