//
//  DataModel.swift
//  TestProject1
//
//  Created by Souvik on 09/01/21.
//

import Foundation

// MARK: - NewOrdersListModel
class RoleListDataModel: Codable {
    var status: Bool?
    var code: Int?
    var data: [Roles]?
    var message: String?

    init(status: Bool?, code: Int?, data: [Roles]?, message: String?) {
        self.status = status
        self.code = code
        self.data = data
        self.message = message
    }
}

// MARK: - Datum
class Roles: Codable {
    var id: Int?
    var roleName: String?
    var roleType: Int?
    var company: JSONNull?
    var colour, order: String?
    var userID: Int?
    var createdAt, updatedAt: String?
    var deletedAt: JSONNull?
    var planning: Planning?

    enum CodingKeys: String, CodingKey {
        case id
        case roleName = "role_name"
        case roleType = "role_type"
        case company, colour, order
        case userID = "user_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case planning
    }

    init(id: Int?, roleName: String?, roleType: Int?, company: JSONNull?, colour: String?, order: String?, userID: Int?, createdAt: String?, updatedAt: String?, deletedAt: JSONNull?, planning: Planning?) {
        self.id = id
        self.roleName = roleName
        self.roleType = roleType
        self.company = company
        self.colour = colour
        self.order = order
        self.userID = userID
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
        self.planning = planning
    }
}

// MARK: - Planning
class Planning: Codable {
    var id: Int?
    var mission, vision: JSONNull?
    var type, clientRoleID: Int?
    var year, quarter, createdAt, updatedAt: String?
    var deletedAt: JSONNull?
    var objective: [Objective]?

    enum CodingKeys: String, CodingKey {
        case id, mission, vision, type
        case clientRoleID = "client_role_id"
        case year, quarter
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case objective
    }

    init(id: Int?, mission: JSONNull?, vision: JSONNull?, type: Int?, clientRoleID: Int?, year: String?, quarter: String?, createdAt: String?, updatedAt: String?, deletedAt: JSONNull?, objective: [Objective]?) {
        self.id = id
        self.mission = mission
        self.vision = vision
        self.type = type
        self.clientRoleID = clientRoleID
        self.year = year
        self.quarter = quarter
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
        self.objective = objective
    }
}

// MARK: - Objective
class Objective: Codable {
    var id: Int?
    var contentObj: String?
    var score: String?
    var projectID: JSONNull?
    var planningID: Int?
    var createdAt, updatedAt: String?
    var deletedAt: JSONNull?
    var keyResult: [KeyResult]?
    var majorAction: [JSONAny]?

    enum CodingKeys: String, CodingKey {
        case id
        case contentObj = "content_obj"
        case score
        case projectID = "project_id"
        case planningID = "planning_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case keyResult = "key_result"
        case majorAction = "major_action"
    }

    init(id: Int?, contentObj: String?, score: String?, projectID: JSONNull?, planningID: Int?, createdAt: String?, updatedAt: String?, deletedAt: JSONNull?, keyResult: [KeyResult]?, majorAction: [JSONAny]?) {
        self.id = id
        self.contentObj = contentObj
        self.score = score
        self.projectID = projectID
        self.planningID = planningID
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
        self.keyResult = keyResult
        self.majorAction = majorAction
    }
}

// MARK: - KeyResult
class KeyResult: Codable {
    var id: Int?
    var keyResult, metrics: String?
    var objectiveID: Int?
    var createdAt, updatedAt: String?
    var deletedAt: JSONNull?
    var type: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case keyResult = "key_result"
        case metrics
        case objectiveID = "objective_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case deletedAt = "deleted_at"
        case type
    }

    init(id: Int?, keyResult: String?, metrics: String?, objectiveID: Int?, createdAt: String?, updatedAt: String?, deletedAt: JSONNull?, type: Int?) {
        self.id = id
        self.keyResult = keyResult
        self.metrics = metrics
        self.objectiveID = objectiveID
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.deletedAt = deletedAt
        self.type = type
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
